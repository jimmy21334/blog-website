const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const dotenv = require('dotenv');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const flash = require('connect-flash');
const seesion = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const fs = require('fs');
const app = express();

// Load routes
const posts = require('./routes/posts');
const users = require('./routes/users');
const index = require('./routes/index');

// Load config
dotenv.config({path: './config/config.env'})

// Static Folder
app.use(express.static('public'));
app.use(express.static('images'));
app.use(express.static('uploads'));

// Load User and post Model 
require('./models/User');
require('./models/Posts');


// Passport Config 
require('./config/passport')(passport);


// Handlebars Helpers 
const {
    formatDate,
    select,
    editIcon
} = require('./helpers/hbs');


// Map Global promise -get rid of warning
mongoose.Promise = global.Promise;

// Connect to mongoose
mongoose.connect('mongodb://localhost/social-network' || process.env.MONGO_URI, {
    useNewUrlParser: true,
    // useUnifiedTopology: true
    useUnifiedTopology: true
}).then(function () {
    console.log('MongoDB Connected...')
}).catch(err => console.log(err));

// Handlebars Middleware
app.engine('handlebars', exphbs({
    helpers: {
        formatDate: formatDate,
        select: select,
        editIcon: editIcon
    },
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

// Body Parser Middleware 
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());


// Method-Override Middleware
app.use(methodOverride('_method'));

// Express Session Middleware
app.use(seesion({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
}));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

// Flash Middlewar
app.use(flash());

// Global varibales
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

// Routes 
app.use('/posts', posts);
app.use('/users', users);
app.use('/', index);

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`Server Started on Port ${port}`);
});